﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Scalar
    {
        public int value { get; set; }
        public new string ToString() => $"{this.value}";
    }
}
