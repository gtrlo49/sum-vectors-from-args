﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class InputAdapter
    {
        private const string InvalidArgumentsMessage = "Vector Arguments are not valid, the correct Format is x1,y2 x2,y2";
        private const string InvalidNumberMessage = "Vector Arguments must supply valid numbers.";
        private const string InvalidVectorCreate = "We had an error when building your vectors, check the format x1,y2 or x1,y1,z1";
        public Vector VectorA { get; private set; }
        public Vector VectorB { get; private set; }

        //public String Operator;

        public InputAdapter(string[] args)
        {
            if (!ValidateVectorFormat(args))
            {
                var argumentsA = args[0].Split(',');
                var argumentsB = args[1].Split(',');

                this.VectorA = CreateVector(argumentsA);
                this.VectorB = CreateVector(argumentsB);
                //this.Operator = args[2];

            }
            else
            {
                throw new ArgumentException(InvalidArgumentsMessage);
            }
        }

        private bool ValidateVectorFormat(string[] arguments)
        {
            if (arguments.Length != 2)
            {
                throw new ArgumentException(InvalidArgumentsMessage);
            }
            return false;
        }

        private Vector CreateVector(string[] arguments)
        {
            switch(arguments.Length)
            {
                case 2:
                    return new Vector(TryGetInteger(arguments[0]), TryGetInteger(arguments[1]));
                case 3:
                    return new Vector(TryGetInteger(arguments[0]), TryGetInteger(arguments[1]), TryGetInteger(arguments[2]));
            }
            throw new ArgumentException(InvalidVectorCreate);
        }

        private int TryGetInteger(string value)
        {
            if (!int.TryParse(value, out var parsedValue))
            {
                throw new ArgumentException(InvalidNumberMessage);
            }
            return parsedValue;
        }
    }
}
