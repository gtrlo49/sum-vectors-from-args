﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Vector : Point
    {
        public int size;
        public Vector(int x, int y)
        {
            this.X = x;
            this.Y = y;
            this.Z = 0;
            this.size = 2;
        }
        public Vector(int x, int y, int z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.size = 3;
        }
        public static Vector operator +(Vector a, Vector b)
        {
            return a.size==2 && b.size==2 ? new Vector(a.X + b.X, a.Y + b.Y) : new Vector(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }
        public static Scalar operator *(Vector a, Vector b)
        {
            return new Scalar
            {
                value = a.X * b.X + a.Y * b.Y + a.Z * b.Z
            };
        }
        public new string ToString()
        {
            switch (this.size)
            {
                case 2:
                    return $"{this.X},{this.Y}";
                case 3:
                    return $"{this.X},{this.Y},{this.Z}";
            }
            return "";
        }
        public double Lenght()
        {
            double aux = (this.X * this.X) + (this.Y * this.Y) + (this.Z * this.Z);
            return Math.Sqrt(aux);
        }
    }
}
