﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var adapter = new InputAdapter(args);
                var vectorA = adapter.VectorA;
                var vectorB = adapter.VectorB;
                var vectorResultAdd = VectorOperator.AddVectors(vectorA, vectorB);
                var vectorResultMultiply = VectorOperator.MultiplicateVectors(vectorA, vectorB);
                var vectorResultLenght = VectorOperator.Lenght(vectorResultAdd);
                Console.WriteLine("");
                Console.WriteLine("!!!!!!!!!!!!RESULTS!!!!!!!!!");
                Console.WriteLine("============================");
                Console.WriteLine($"Result: {vectorResultAdd.ToString()}");
                Console.WriteLine($"Result: {vectorResultMultiply.ToString()}");
                Console.WriteLine($"Result: {vectorResultLenght.ToString()}");
            }
            catch (System.ArgumentException ae)
            {
                Console.WriteLine("");
                Console.WriteLine("WARNING!!!!!!!");
                Console.WriteLine("==================");
                Console.WriteLine(ae.Message);
            }
            
        }
    }
}
