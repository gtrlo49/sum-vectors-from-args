using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp1;

namespace UnitTestProject1
{
    [TestClass]
    public class NegativeTests
    {
        [TestMethod]
        public void ValidateDimmentionsFromVectors()
        {
            string[] args = { "2,1" , "1" };
            Assert.ThrowsException<System.ArgumentException>(() =>new InputAdapter(args));
        }
        [TestMethod]
        public void ValidateTypeFromVectors()
        {
            string[] args = { "2,1", "asd" };
            Assert.ThrowsException<System.ArgumentException>(() => new InputAdapter(args));
        }
        [TestMethod]
        public void ValidateLenghtFromArgs()
        {
            string[] args = { "2,1", "1,5", "1,5" };
            Assert.ThrowsException<System.ArgumentException>(() => new InputAdapter(args));
        }
    }
}
