﻿using ConsoleApp1;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace UniTest.Project.Vectors
{
    [TestClass]
    public class PossitiveTests
    {
        [TestMethod]
        public void ValidateSum2D()
        {
            string[] args = { "2,1", "1,5" };
            var adapter = new InputAdapter(args);
            var VectorA = adapter.VectorA;
            var VectorB = adapter.VectorB;
            var result = VectorOperator.AddVectors(VectorA, VectorB);
            Assert.AreEqual(result.ToString(), "3,6");
        }
        [TestMethod]
        public void ValidateSum3D()
        {
            string[] args = { "2,-1,5", "1,8,-4" };
            var adapter = new InputAdapter(args);
            var VectorA = adapter.VectorA;
            var VectorB = adapter.VectorB;
            var result = VectorOperator.AddVectors(VectorA, VectorB);
            Assert.AreEqual(result.ToString(), "3,7,1");
        }
        [TestMethod]
        public void ValidateLenght2D()
        {
            string[] args = { "2,-1,5", "1,8,-4" };
            var adapter = new InputAdapter(args);
            var VectorA = adapter.VectorA;
            var VectorB = adapter.VectorB;
            var result = VectorOperator.AddVectors(VectorA, VectorB);
            var lenght = VectorOperator.Lenght(result);
            Assert.AreEqual(lenght.ToString(), "7.68114574786861");
        }
        [TestMethod]
        public void ValidateLenght3D()
        {
            string[] args = { "2,-1,5", "1,8,4" };
            var adapter = new InputAdapter(args);
            var VectorA = adapter.VectorA;
            var VectorB = adapter.VectorB;
            var result = VectorOperator.AddVectors(VectorA, VectorB);
            var lenght = VectorOperator.Lenght(result);
            Assert.AreEqual(lenght.ToString(), "11.7898261225516");
        }
        [TestMethod]
        public void ValidateDotProduct2D()
        {
            string[] args = { "2,-1", "1,8" };
            var adapter = new InputAdapter(args);
            var VectorA = adapter.VectorA;
            var VectorB = adapter.VectorB;
            var result = VectorOperator.MultiplicateVectors(VectorA, VectorB);
            Assert.AreEqual(result.ToString(), "-6");
        }
        [TestMethod]
        public void ValidateDotProduct3D()
        {
            string[] args = { "2,-1,5", "1,8,4" };
            var adapter = new InputAdapter(args);
            var VectorA = adapter.VectorA;
            var VectorB = adapter.VectorB;
            var result = VectorOperator.MultiplicateVectors(VectorA, VectorB);
            Assert.AreEqual(result.ToString(), "14");
        }
    }
}
