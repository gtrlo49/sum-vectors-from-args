﻿using System;

namespace Exercise1
{
    class Program
    {
        public static System.Array ResizeArray(System.Array oldArray, int newSize)
        {
            int oldSize = oldArray.Length;
            System.Type elementType = oldArray.GetType().GetElementType();
            System.Array newArray = System.Array.CreateInstance(elementType, newSize);
            int preserveLength = System.Math.Min(oldSize, newSize);
            if (preserveLength > 0)
            {
                System.Array.Copy(oldArray, newArray, preserveLength);
            }
            return newArray;
        }

        public string[] separeArgs(string value, ref int[] coordenates)
        {
            string[] aux = value.Split(',');
            if (coordenates.Length < value.Length)
            {
                coordenates = (int[])ResizeArray(coordenates, aux.Length);
            }
            return aux;
        }

        public void addToLast(string[] value, ref int[] coordenates)
        {
            for (int i = 0; i < value.Length; i++)
            {
                coordenates[i] += Int32.Parse(value[i]);
            }
        }

        public int[] Sumvectors(string[] values)
        {
            int[] coordenates=new int[1];
            foreach (var item in values)
            {
                string[] value = separeArgs(item, ref coordenates);
                addToLast(value, ref coordenates);
            }
            return coordenates;
        }

        public void showVector(int[] vector)
        {
            for (int i = 0; i < vector.Length; i++)
            {
                Console.Write(vector[i]);
                if (i < vector.Length - 1)
                {
                    Console.Write(',');
                }
            }
        }


        static void Main(string[] args)
        {
            Program program = new Program();

            int[] vector = program.Sumvectors(args);

            Console.WriteLine("El vector resultante es:");

            program.showVector(vector);

        }
    }
}
